import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import thunk from 'redux-thunk';
import {applyMiddleware, createStore} from "redux";
import {Provider} from "react-redux";
import DishesView from "./components/DishesView/DishesView";
import reducer from "./store/reducer";
import axios from 'axios';

axios.defaults.baseURL = 'https://getpizzaapp.firebaseio.com';

export default class App extends React.Component {
    render() {
        const store = createStore(reducer, applyMiddleware(thunk));

        return (
            <View style={styles.container}>
                <View style={styles.logo}><Text style={styles.logoText}>Turtle Pizza</Text></View>
                <Provider store={store}>
                    <DishesView/>
                </Provider>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    // container: {
    //     flex: 1,
    //     backgroundColor: '#fff',
    //     alignItems: 'center',
    //     justifyContent: 'center',
    // },
    logo: {
        // position: 'fixed',
        margin: 25,
        borderBottomWidth: 2,
        paddingBottom: 10,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    logoText: {
        fontSize: 35

    }
});
