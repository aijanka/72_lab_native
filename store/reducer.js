import * as actionTypes from './actionTypes';

const initialState = {
    dishes: [],
    orders: [],
    loading: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.DISHES_REQUEST:
            return {...state, loading: true};
        case actionTypes.DISHES_SUCCESS:
            return {...state, dishes: action.dishes, loading: false};
        case actionTypes.DISHES_FAILURE:
            return {...state, error: action.error, loading: false};

        default :
            return state;
    }
}

export default reducer;