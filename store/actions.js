import axios from 'axios';
import * as actionTypes from './actionTypes';

export const dishesRequest = () => ({type: actionTypes.DISHES_REQUEST});
export const dishesSuccess = dishes => ({type: actionTypes.DISHES_SUCCESS, dishes});
export const dishesFailure = error => ({type: actionTypes.DISHES_FAILURE, error});

export const loadDishes = () => {
    return (dispatch, getState) => {
        dispatch(dishesRequest());
        axios.get('/dishes.json').then(response => {
            dispatch(dishesSuccess(response.data));
        }, error => {
            dispatch(dishesFailure(error));
        })
    }
};