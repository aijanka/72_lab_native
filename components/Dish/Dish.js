import React from 'react';
import {Image, Text, TouchableOpacity, View} from "react-native";

const Dish = props => (
    <TouchableOpacity onPress={props.clicked} style={styles.dishWrapper}>
        <Image
            style={{width: 66, height: 58}}
            source={{uri: props.img}}
        />
        <View style={styles.dishText}>
            <Text>{props.title}</Text>
            <Text>{props.price}</Text>

        </View>
    </TouchableOpacity>
);

const styles = {
    dishWrapper: {
        margin: 40,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    dishText: {
        marginLeft: 20
    }
}

export default Dish;