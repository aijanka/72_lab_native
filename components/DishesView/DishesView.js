import React from 'react';
import {Image, Modal, Text, TouchableOpacity, View} from "react-native";
import {loadDishes} from "../../store/actions";
import Dish from "../Dish/Dish";
import {connect} from "react-redux";


class DishesView extends React.Component {
    state = {
        showAddModal: false,
        isEditing: false,
        currentKey: '',
        dish: {
            title: '',
            price: '',
            image: ''
        }
    }

    componentDidMount(){
        this.props.loadDishes();
    }

    showModal = (key) => {
        const d = this.props.dishes;
        const dish = {
            title: d[key].title,
            price: d[key].price,
            image: d[key].image
        };
        this.setState({dish});
        this.toggleModal();
    }

    toggleModal = () => {
        this.setState(prevState => ({showAddModal: !prevState.showAddModal}));
    };

    render() {
        const keys = this.props.dishes ? Object.keys(this.props.dishes) : [];
        return (
            <View>
                <Modal
                    animationType="slide"
                    // transparent={false}
                    visible={this.state.showAddModal}
                    onRequestClose={this.toggleModal}>
                    <View style={styles.modalContainer}>
                        <Image style={styles.modalImage} source={{uri: this.state.dish.image}}/>
                        <Text>{this.state.dish.title}</Text>
                        <Text>{this.state.dish.price}</Text>

                        <TouchableOpacity
                            onPress={this.toggleModal}
                            style={styles.modalButton}
                        >
                            <Text style={{color: 'white'}}>Hide Modal</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>

                {keys.map(key => (
                    <Dish
                        key={key}
                        title={this.props.dishes[key].title}
                        price={this.props.dishes[key].price}
                        img={this.props.dishes[key].image}
                        clicked={() => this.showModal(key)}
                    />
                ))}
            </View>
        )
    }
}

const styles = {
    modalContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalImage: {
        width: 200,
        height: 200
    },
    modalButton: {
        height: 50,
        borderWidth: .5,
        borderColor: 'rgb(0,129,182)',
        backgroundColor: 'rgb(0,129,182)',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10,
        padding: 10
    }
}

const mapStateToProps = state => ({
    dishes: state.dishes
});

const mapDispatchToProps = dispatch => ({
    loadDishes: () => dispatch(loadDishes()),
});

export default connect(mapStateToProps, mapDispatchToProps)(DishesView);
